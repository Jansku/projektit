﻿using System;

class Program
{
    static void Main(string[] args)
    {
        // Aloita peli
        Console.WriteLine("Paina mitä tahansa näppäintä jatkaaksesi...");
        Console.ReadKey();

        
        char ch = '*';
        bool gameLive = true;
        ConsoleKeyInfo consoleKey; 

        // location info & display
        int x = 0, y = 2; 
        int dx = 1, dy = 0;
        int consoleWidthLimit = 79;
        int consoleHeightLimit = 24;

        
        Console.BackgroundColor = ConsoleColor.DarkGray;
        Console.Clear();

        
        int delayInMillisecs = 50;

        
        bool trail = true;

        do 
        {
            
            ConsoleColor cc = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Nuolinäppäimet ylös/alas/oikea/vasen. 't' reitti.  'c' puhdista näyttö  'esc' lopeta.");
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = cc;

            
            if (Console.KeyAvailable)
            {
                
                consoleKey = Console.ReadKey(true);
                switch (consoleKey.Key)
                {
                    case ConsoleKey.T:
                        trail = true;
                        break;
                    case ConsoleKey.C:
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                        trail = true;
                        Console.Clear();
                        break;
                    case ConsoleKey.UpArrow: //YLÖS
                        dx = 0;
                        dy = -1;
                        Console.ForegroundColor = ConsoleColor.Red;
                        break;
                    case ConsoleKey.DownArrow: // ALAS
                        dx = 0;
                        dy = 1;
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        break;
                    case ConsoleKey.LeftArrow: //VASEN
                        dx = -1;
                        dy = 0;
                        Console.ForegroundColor = ConsoleColor.Green;
                        break;
                    case ConsoleKey.RightArrow: //OIKEA
                        dx = 1;
                        dy = 0;
                        Console.ForegroundColor = ConsoleColor.Black;
                        break;
                    case ConsoleKey.Escape: //LOPPU
                        gameLive = false;
                        break;
                }
            }

            
            Console.SetCursorPosition(x, y);
            if (trail == false)
                Console.Write(' ');

            
            x += dx;
            if (x > consoleWidthLimit)
                x = 0;
            if (x < 0)
                x = consoleWidthLimit;

            y += dy;
            if (y > consoleHeightLimit)
                y = 2; 
            if (y < 2)
                y = consoleHeightLimit;

            
            Console.SetCursorPosition(x, y);
            Console.Write(ch);

            
            System.Threading.Thread.Sleep(delayInMillisecs);

        } while (gameLive);
    }
}


