﻿using System;

namespace Harj09
{
    enum Appstatus
    {
        Maanantai,
        Tiistai,
        Keskiviikko,
        Torstai,
        Perjantai,
        Lauantai,
        Sunnuntai,
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Harj09");

            Appstatus status = Appstatus.Maanantai;
            switch (status)
            {
                case Appstatus.Maanantai:
                    Console.WriteLine("Maanantai");
                    break;
                case Appstatus.Tiistai:
                    Console.WriteLine("Tiistai");
                    break;
                case Appstatus.Keskiviikko:
                    Console.WriteLine("Keskiviikko");
                    break;
                case Appstatus.Torstai:
                    Console.WriteLine("Torstai");
                    break;
                case Appstatus.Perjantai:
                    Console.WriteLine("Perjantai");
                    break;
                case Appstatus.Lauantai:
                    Console.WriteLine("Lauantai");
                    break;
                case Appstatus.Sunnuntai:
                    Console.WriteLine("Sunnuntai");
                    break;
            }

            {
                Ajoneuvot classic;
                classic.Merkki = "Toyota Corolla";
                classic.Valmistaja = "Toyota";
                classic.Hinta = 37000;
                classic.Vuosimalli = 2019;
                Console.WriteLine(classic.ToString());
            }

            {
                Ajoneuvot classic;
                classic.Merkki = "Audi A4";
                classic.Valmistaja = "Audi";
                classic.Hinta = 14000;
                classic.Vuosimalli = 2009;
                Console.WriteLine(classic.ToString());
            }

            {
                Ajoneuvot classic;
                classic.Merkki = "Maserati Ghibli";
                classic.Valmistaja = "Maserati";
                classic.Hinta = 80000;
                classic.Vuosimalli = 2017;
                Console.WriteLine(classic);
            }
        }
    }
}
