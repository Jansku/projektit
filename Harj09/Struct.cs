﻿using System;

struct Ajoneuvot
{
    public override string ToString()
    {
        return Merkki + ", " + Valmistaja + ", " + Hinta + ", " + Vuosimalli;
    }
    public string Merkki;
    public string Valmistaja;
    public int Hinta;
    public int Vuosimalli;
}


